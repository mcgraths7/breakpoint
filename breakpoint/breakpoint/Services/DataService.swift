//
//  DataService.swift
//  breakpoint
//
//  Created by Steven McGrath on 9/13/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import Foundation
import Firebase

class DataService {
    static let instance = DataService()

    private var _REF_BASE = DB_BASE
    private var _REF_USERS = DB_BASE.child("users")
    private var _REF_GROUPS = DB_BASE.child("groups")
    private var _REF_FEED = DB_BASE.child("feed")


    var REF_BASE: DatabaseReference {
        return _REF_BASE
    }

    var REF_USERS: DatabaseReference {
        return _REF_USERS
    }

    var REF_GROUPS: DatabaseReference {
        return _REF_GROUPS
    }

    var REF_FEED: DatabaseReference {
        return _REF_FEED
    }

    func createDBUser(uid: String, userData: Dictionary<String, Any>) {
        REF_USERS.child(uid).updateChildValues(userData)
    }


    func uploadPost(withMessage message: String, forUid uid: String, withGroupKey groupKey: String?, completion: @escaping (_ status: Bool) -> Void ) {
        if groupKey != nil {
            // Send to groups ref

        } else {
            REF_FEED.childByAutoId().updateChildValues(["content": message, "senderId": uid])
            completion(true)
        }
    }

    func getAllFeedMessages(completion: @escaping (_ messages: [Message]) -> Void) {
        REF_FEED.observeSingleEvent(of: .value) { (feedMessageSnapshot) in
            var messageArray = [Message]()
            guard let snapshot = feedMessageSnapshot.children.allObjects as? [DataSnapshot] else { return }

            for message in snapshot {
                let content = message.childSnapshot(forPath: "content").value as! String
                let senderId = message.childSnapshot(forPath: "senderId").value as! String
                let newMessage = Message(withContent: content, andSenderId: senderId)
                messageArray.insert(newMessage, at: 0)
            }
            completion(messageArray)
        }
    }

    func getUserName(forUid uid: String, completion: @escaping (_ username: String) -> Void) {
        REF_USERS.observeSingleEvent(of: .value) { (userSnapshot) in
            guard let snapshot = userSnapshot.children.allObjects as? [DataSnapshot] else { return }

            for user in snapshot {
                if user.key == uid {
                    completion(user.childSnapshot(forPath: "email").value as! String)
                }
            }
        }
    }
}
