//
//  AuthService.swift
//  breakpoint
//
//  Created by Steven McGrath on 9/13/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import Foundation
import Firebase

class AuthService {
    static let instance = AuthService()

    private(set) public var isLoggedIn = false

    func registerUser(withEmail email: String, andPassword password: String, userCreationComplete: @escaping (_ status: Bool, _ error: Error?) -> () ) {
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            guard let user = result?.user else {
                userCreationComplete(false, error)
                return
            }
            let userData = ["provider": user.providerID, "email": user.email]
            DataService.instance.createDBUser(uid: user.uid, userData: userData as Dictionary<String, Any>)
            userCreationComplete(true, nil)
        }
    }

    func loginUser(withEmail email: String, andPassword password: String, userLoginComplete: @escaping (_ status: Bool, _ error: Error?) -> () ) {
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if error != nil {
                userLoginComplete(false, nil)
                return
            }
            userLoginComplete(true, nil)
        }
    }
}
