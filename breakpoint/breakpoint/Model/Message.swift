//
//  Message.swift
//  breakpoint
//
//  Created by Steven McGrath on 9/16/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import Foundation

struct Message {
    private var _content: String
    private var _senderId: String

    var content: String {
        return _content
    }
    var senderId: String {
        return _senderId
    }

    init(withContent content: String, andSenderId senderId: String) {
        self._content = content
        self._senderId = senderId
    }
}
