//
//  FeedCell.swift
//  breakpoint
//
//  Created by Steven McGrath on 9/16/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class FeedCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var messageContentLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!

    func configureCell(profileImage: UIImage, email: String, content: String) {
        self.profileImage.image = profileImage
        self.emailLabel.text = email
        self.messageContentLabel.text = content
    }
}
