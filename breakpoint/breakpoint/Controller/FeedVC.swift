//
//  FirstViewController.swift
//  breakpoint
//
//  Created by Steven McGrath on 9/13/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class FeedVC: UIViewController {

    // MARK: Variables
    var messageArray = [Message]()

    // MARK: IBOutlets
    @IBOutlet weak var feedTable: UITableView!

    // MARK: IBActions
    @IBAction func onCreatePostButtonTapped(_ sender: UIButton) {
        let createPost = CreatePostVC()
        createPost.modalPresentationStyle = .custom
        present(createPost, animated: true, completion: nil)
    }

    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        feedTable.delegate = self
        feedTable.dataSource = self
        loadData()
        NotificationCenter.default.addObserver(self, selector: #selector(FeedVC.loadData), name: NSNotification.Name(rawValue: "loadData"), object: nil)
    }

    // MARK: NotificationCenter Observers
    @objc func loadData() {
        DataService.instance.getAllFeedMessages { (messages) in
            self.messageArray = messages
            self.feedTable.reloadData()
        }
    }
}

extension FeedVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = feedTable.dequeueReusableCell(withIdentifier: "FeedCell") as? FeedCell else { return FeedCell() }
        let image = UIImage(named: "defaultProfileImage")
        let message = messageArray[indexPath.row]
        cell.configureCell(profileImage: image!, email: message.senderId, content: message.content)
        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

