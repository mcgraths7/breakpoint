//
//  LoginVC.swift
//  breakpoint
//
//  Created by Steven McGrath on 9/16/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    // MARK: IBOutlets
    @IBOutlet weak var emailTextField: InsetTextField!
    @IBOutlet weak var passwordTextField: InsetTextField!

    // MARK: IBActions
    @IBAction func signInButtonTapped(_ sender: UIButton) {
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        if email != "", password != "" {
            AuthService.instance.loginUser(withEmail: email, andPassword: password) { (success, loginError) in
                if success {
                    print("Login successful")
                    self.dismiss(animated: true, completion: nil)
                } else {
                    print(String(describing: loginError?.localizedDescription))
                }

                AuthService.instance.registerUser(withEmail: email, andPassword: password, userCreationComplete: { (success, registerError) in
                    if success {
                        print("Registration successful")
                        AuthService.instance.loginUser(withEmail: email, andPassword: password, userLoginComplete: { (success, loginError) in
                            if success {
                                print("Login successful")
                                self.dismiss(animated: true, completion: nil)
                            } else {
                                print(String(describing: loginError?.localizedDescription))
                            }
                        })
                    } else {
                        print(String(describing: registerError?.localizedDescription))
                    }
                })
            }
        }
    }
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }


}

extension LoginVC: UITextFieldDelegate { }

