//
//  MeVC.swift
//  breakpoint
//
//  Created by Steven McGrath on 9/16/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class MeVC: UIViewController {

    // MARK: IBOutlets
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var profileTable: UITableView!

    //MARK: IBActions
    @IBAction func signOutButtonTapped(_ sender: UIButton) {

    }

    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
