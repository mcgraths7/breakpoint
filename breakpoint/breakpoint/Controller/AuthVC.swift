//
//  AuthVC.swift
//  breakpoint
//
//  Created by Steven McGrath on 9/16/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class AuthVC: UIViewController {

    // MARK: IBActions
    @IBAction func signInWithEmailButtonTapped(_ sender: UIButton) {
        guard let LoginVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") else { return }
        present(LoginVC, animated: true, completion: nil)
    }

    @IBAction func signInWithFacebookButtonTapped(_ sender: UIButton) {
    }

    @IBAction func signInWithGoogleButtonTapped(_ sender: UIButton) {
    }

    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
