//
//  CreatePostVC.swift
//  breakpoint
//
//  Created by Steven McGrath on 9/16/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit
import Firebase

class CreatePostVC: UIViewController {

    // MARK: IBOutlets
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var postTextView: UITextView!
    @IBOutlet weak var sendButton: UIButton!

    // MARK: IBActions
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func sendButtonTapped(_ sender: UIButton) {
        if let message = postTextView.text, message != "Say something here..." {
            sendButton.isEnabled =  false
            guard let currentUser = Auth.auth().currentUser else { return }
            let uid = currentUser.uid
            DataService.instance.uploadPost(withMessage: message, forUid: uid, withGroupKey: nil) { (success) in
                if success {
                    self.sendButton.isEnabled = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadData"), object: nil)
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.sendButton.isEnabled = true
                    print("An error has occurred...")
                }
            }
        }

    }

    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        postTextView.delegate = self
        emailLabel.text = Auth.auth().currentUser?.email ?? "user@breakpoint.com"
        sendButton.bindToKeyboard()
    }
}

extension CreatePostVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        postTextView.text = ""
    }
}

